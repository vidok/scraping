require 'sinatra'
require 'haml'

require './lib/apple_warranty_parser'
require './lib/device'

set :server, :puma
set :haml, format: :html5, attr_wrapper: '"'

get '/devices/:imei' do
  begin
    @device = Device.new(params[:imei])
    haml :show
  rescue AppleWarrantyParser::ParseError => @error
    haml :error
  end
end
