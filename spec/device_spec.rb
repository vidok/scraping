require 'spec_helper'
require './lib/device'

describe Device do

  let(:active_warranty) { {
      'device_model' => 'iPhone 5c',
      'phoneSupportHasCoverage' => true,
      'hwSupportHasCoverage' => true,
      'phone_support_expired_date' => '25 June, 2018',
      'hardware_support_expired_date' => '25 June, 2018'
    }
  }

  let(:inactive_warranty) { {
      'device_model' => 'iPhone 5c',
      'phoneSupportHasCoverage' => false,
      'hwSupportHasCoverage' => false
    }
  }

  before(:example) { allow_any_instance_of(Device).to receive(:init_warranty_data) }

  it "#initialize without imei" do
    expect{ Device.new }.to raise_error
  end

  context "active warranty" do

    before(:example) {
      allow_any_instance_of(Device).to receive(:init_warranty_data)
      allow_any_instance_of(Device).to receive(:warranty_data).and_return(active_warranty)
      @device = Device.new(rand(10000))
    }

    it "#model" do
      expect(@device.model).to eq('iPhone 5c')
    end

    it "#has_phone_support" do
      expect(@device.phone_support?).to eq true
      expect(@device.phone_support_expired_date).not_to be_nil
    end

    it "#has_hardware_support" do
      expect(@device.hardware_support?).to eq true
      expect(@device.hardware_support_expired_date).not_to be_nil
    end
  end

  context "inactive warranty" do

    before(:example) {
      allow_any_instance_of(Device).to receive(:warranty_data).and_return(inactive_warranty)
      @device = Device.new(rand(10000))
    }

    it "#has_phone_support" do
      expect(@device.phone_support?).to eq false
      expect(@device.phone_support_expired_date).to be_nil
    end

    it "#has_hardware_support" do
      expect(@device.hardware_support?).to eq false
      expect(@device.hardware_support_expired_date).to be_nil
    end
  end

end
