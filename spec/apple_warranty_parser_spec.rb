require 'spec_helper'
require './lib/apple_warranty_parser'

describe AppleWarrantyParser do
  let(:dummy_class) { Class.new { include AppleWarrantyParser } }

  let(:bad_response) { File.read('spec/fixtures/bad_response.html') }
  let(:success_response) { File.read('spec/fixtures/success_response.html') }
  let(:response_with_expired_warranty) { File.read('spec/fixtures/success_response_with_expired_warranty.html') }

  before(:example) do
    @dummy = dummy_class.new
    @dummy.instance_eval { @imei = rand(100000) }
  end

  context "bad request" do
    it "#warranty_data raise parse error" do
      stub_request(:post, "https://selfsolve.apple.com/wcResults.do")
         .to_return(:status => 200, :body => bad_response, :headers => {})
      expect{ @dummy.warranty_data }.to raise_error(AppleWarrantyParser::ParseError)
    end
  end

  context "success request" do
    it "#warranty_data is active" do
      stub_request(:post, "https://selfsolve.apple.com/wcResults.do")
         .to_return(:status => 200, :body => success_response, :headers => {})
      expect(@dummy.warranty_data['phoneSupportHasCoverage']).to eq true
      expect(@dummy.warranty_data['hwSupportHasCoverage']).to eq true
      expect(@dummy.warranty_data['hardware_support_expired_date']).not_to be_nil
      expect(@dummy.warranty_data['phone_support_expired_date']).not_to be_nil
      expect(@dummy.warranty_data.size).to eq(5) # now parse 5 properties
    end

    it "#warranty_data is active warranty" do
      stub_request(:post, "https://selfsolve.apple.com/wcResults.do")
         .to_return(:status => 200, :body => response_with_expired_warranty, :headers => {})
      expect(@dummy.warranty_data['phoneSupportHasCoverage']).to eq false
      expect(@dummy.warranty_data['hwSupportHasCoverage']).to eq false
      expect(@dummy.warranty_data.size).to eq(3) # now parse 3 properties
    end
  end
end
