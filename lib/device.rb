# Device model
class Device
  include AppleWarrantyParser

  attr_reader :imei

  def initialize(imei)
    @imei = imei
    init_warranty_data
  end

  def model
    warranty_data['device_model']
  end

  def phone_support?
    warranty_data['phoneSupportHasCoverage']
  end

  def hardware_support?
    warranty_data['hwSupportHasCoverage']
  end

  def phone_support_expired_date
    warranty_data['phone_support_expired_date']
  end

  def hardware_support_expired_date
    warranty_data['hardware_support_expired_date']
  end
end
