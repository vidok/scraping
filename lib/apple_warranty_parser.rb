require 'net/http'
require 'uri'

# module for scraping data from https://selfsolve.apple.com/wcResults.do by imei
module AppleWarrantyParser
  class ParseError < StandardError; end
  class NetworkError < StandardError; end

  AVAILABLE_PROPS = %w(hwSupportHasCoverage phoneSupportHasCoverage)

  def warranty_data
    unless @warranty_data
      request_to_apple @imei
      @warranty_data = { 'device_model' =>  parse_device_model }
      @warranty_data.merge! parse_main_json_object
      convert_string_boolean

      if @warranty_data['hwSupportHasCoverage']
        hardware_date = parse_hardware_support_expired_date
        @warranty_data['hardware_support_expired_date'] = hardware_date if hardware_date
      end

      if @warranty_data['phoneSupportHasCoverage']
        phone_date = parse_phone_support_expired_date
        @warranty_data['phone_support_expired_date'] = phone_date if hardware_date
      end
    end
    @warranty_data
  end

  alias_method :init_warranty_data, :warranty_data

  private

  def request_to_apple imei
    uri = URI.parse('https://selfsolve.apple.com/wcResults.do')

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(uri.request_uri)
    request.set_form_data({ sn: imei, num: rand(10000) })

    response = http.request(request)
    @html = response.body.delete("\n") # remove \n for simplify regexp
  rescue StandardError
    raise NetworkError, 'Apple service is not available'
  end

  # converting strings "true" or "false" to boolean
  def convert_string_boolean
    AVAILABLE_PROPS.each do |property|
      @warranty_data[property] = @warranty_data[property] == 'true'
    end
  end

  # find data in string by regexp and return one match result
  def find_by regexp, match_index = 1
    result = regexp.match(@html)
    result[match_index] if result
  end

  def parse_device_model
    find_by %r{displayProductInfo\(\'[a-z\/\:\.\?\=\&0-9]+\'\,\s\'([0-9a-z\s]+)}i
  end

  def parse_hardware_support_expired_date
    find_by %r{HWSupportInfo\([\w\,\s\'\:\(\)\+\.]+\<br\/\>[\w\s]+\:\s([\w\s\,]+)}
  end

  def parse_phone_support_expired_date
    find_by %r{PHSupportInfo\([\w\,\s\'\:\(\)\+\.]+\<br\/\>[\w\s]+\:\s([\w\s\,]+)}
  end

  # parse main json object
  def parse_main_json_object
    match_result = find_by %r{var\sjsonObj\s=\s\{([\'a-z\s\:\,0-9]+)\}}i
    parts = match_result.split(',').map { |r| r.gsub(/\'/, '').strip }

    data = {}
    parts.each do |row|
      key, value = row.split(':').map(&:strip)
      data[key] = value if AVAILABLE_PROPS.include? key
    end
    data
  rescue StandardError
    raise ParseError, 'Device not found or bad response'
  end

  # //Begin:Added json Object:Espressp #14774083
  #         var jsonObj = {
  #                'phoneSupportHasCoverage' : true,
  #                'hwSupportHasCoverage' :    true,
  #                'hwSupportCoverageValue' : 	'PD',
  #                'hasActiveAPP':             false,
  #                'isAPPEligible' :           false,
  #                'isPPIEligible' :           false,
  #                'productType' :             'iPhone',
  #                'numDaysSinceDOP':          '297',
  #                'hasTechToolDownload':       false
  #          };
end
