require './lib/apple_warranty_parser'
require './lib/device'

imei = ARGV[0]

raise RuntimeError, 'IMEI not set' unless imei

device = Device.new imei

puts "Device model: #{device.model}"

puts "Telephone Support: #{(device.phone_support?) ?
      "Active. Expired in #{device.phone_support_expired_date}": 'Expired'}"

puts "Service Coverage: #{(device.hardware_support?) ?
  "Active. Expired in #{device.hardware_support_expired_date}": 'Expired'}"
